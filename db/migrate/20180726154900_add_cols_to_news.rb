class AddColsToNews < ActiveRecord::Migration[5.2]
  def change
    add_column :news, :date, :datetime
    add_column :news, :title, :string
    add_column :news, :content, :string
    add_column :news, :author, :string
    add_column :news, :image, :string
  end
end
