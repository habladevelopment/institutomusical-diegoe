class AddTranslationToPost < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :title_en, :string
    add_column :posts, :content_en, :string
  end
end
