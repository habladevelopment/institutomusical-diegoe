class ParentLessonsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_lesson, only: [:show, :edit, :update, :destroy]

  layout 'admin', only: [:index_admin, :new, :edit]
  def index_admin
    @lessons = ParentLesson.all
  end

  def show
  end

  def new
    @lesson = ParentLesson.new
  end

  def edit
  end

  def create_record
    @lesson = ParentLesson.new(lesson_params)

    respond_to do |format|
      if @lesson.save
        format.html { redirect_to parents_school_lessons_admin_path, notice: 'lesson was successfully created.' }
        format.json { render :show, status: :created, location: @lesson }
      else
        format.html { render :new }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @lesson.update(lesson_params)
        format.html { redirect_to parents_school_lessons_admin_path, notice: 'lesson was successfully updated.' }
        format.json { render :show, status: :ok, location: @lesson }
      else
        format.html { render :edit }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @lesson.destroy
    respond_to do |format|
      format.html { redirect_to parents_school_lessons_admin_path, notice: 'Lesson was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lesson
      @lesson = ParentLesson.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lesson_params
      params.require(:parent_lesson).permit(:video, :video_en, :name_es, :name_en, :thumbnail )
    end
end
