class NewsController < ApplicationController
  #before_action :authenticate_user!

    def index
        @news = if params[:search]
            Post.search(params[:search])
        elsif params[:category]
            category_id = params[:category]
            logger.info("----- CATEGORY ID -----")
            logger.info(category_id)
            @category_name = CategoryPost.find(category_id).name
            Post.where(category_post_id: category_id)
        else
            Post.all.order(created_at: :desc)
        end
    end
    
    def show
        @post = Post.friendly.find(params[:id])
        @recents = Post.limit(3).order(created_at: :desc)
        @categories = CategoryPost.all
    end

    def search
        @news = Post.search(params[:term])
        render json: { results: @news, locale: I18n.locale }
    end
end
