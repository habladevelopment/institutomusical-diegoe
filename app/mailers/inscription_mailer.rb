class InscriptionMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.inscription_mailer.applicant.subject
  #
  def applicant (inscription)
    @inscription = inscription
    emails=[]
    emails.push(@inscription.mother_email) unless @inscription.mother_email == ""
    emails.push(@inscription.father_email) unless @inscription.father_email == ""
    mail(to: emails,subject: "Tus datos han sido recibidos")
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.inscription_mailer.institution.subject
  #
  def institution (inscription)
    @inscription = inscription
    
    @emails_to_send = Setup.first.emails_notification_inscription.split(",").map(&:strip)
    mail(to: @emails_to_send, subject: "Nuevo aplicante para el instituto")
  end

  def priority (inscription)
    @inscription = inscription
    emails = Setup.first.emails_notification_priority.split(",").map(&:strip)
    mail(to: emails, subject: "[PRIORITARIO] Nuevo aspirante para el instituto")
  end

end
