class Post < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: :slugged
  resourcify
  
  has_attached_file :image
	validates :image, attachment_presence: false
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  
  
  belongs_to :category_post
  
  def self.search(term)
    if term
      where('lower(title) LIKE ?', "%#{term.downcase}%").order(created_at: :desc)
    else
      all
    end
  end
end
