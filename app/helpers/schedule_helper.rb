module ScheduleHelper
  require 'date'

  def events_by_month()
    events = Schedule.all.order(:date_event)
    grouped = events.group_by {|s| s.date_event.to_date.month}
    return grouped
  end

  def find_event(date, events)
    found = nil
    unless events.nil?
      events.each do |event|
        if event.date_event == date
          found = event
          break
        end
      end
    end
    found
  end

  def all_days_in_month(month)
    now = Date.today
    begins = Date.new(now.year, month, 1)
    ends = begins.end_of_month()
    range = (begins..ends)
  end

  def is_actual_month(month)
    now = Date.today
    if now.month == month
      return true
    else
      return false
    end
  end

  def month_name_year(month)
    now = Date.today
    begins = Date.new(now.year, month, 1)
  end

  def schedule_description(schedule)
    if (@lang.to_s == 'es')
      description = schedule.description
    else
      description = schedule.description_en
    end
    description
  end
end
