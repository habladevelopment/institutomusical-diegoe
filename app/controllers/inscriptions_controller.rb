class InscriptionsController < ApplicationController
  before_action :get_config, only: [:index]
  def index
    @inscription = Inscription.new
  end

  def create
    @inscription = Inscription.new(inscription_params)
		if @inscription.valid?
      @inscription.save
      InscriptionMailer.applicant(@inscription).deliver_now
      if params["apply_to_school_check"] == "selected"
        logger.info "------- Primaria ---------"
        InscriptionMailer.priority(@inscription).deliver_now
      else
        InscriptionMailer.institution(@inscription).deliver_now
      end
      redirect_to inscriptions_thank_you_path
    end
  end

  private
  def inscription_params
    params.require(:inscription).permit(:city, :neighborhood, :address,:phone, :another_phone, :change_reason, :who_recommended,
                  :student_name, :student_last_school, :student_birthdate, :student_ingress_year,
                  :mother_name, :mother_age, :mother_school, :mother_profession, :mother_university, :mother_workplace, :mother_job,
                  :mother_email, :mother_phone, :mother_cellphone, :father_name, :father_age, :father_school, :father_profession, 
                  :father_university, :father_workplace, :father_job, :father_email, :father_phone, :father_cellphone)
  end
end
