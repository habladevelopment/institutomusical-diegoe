require 'test_helper'

class InscriptionMailerTest < ActionMailer::TestCase
  test "applicant" do
    mail = InscriptionMailer.applicant
    assert_equal "Applicant", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "institution" do
    mail = InscriptionMailer.institution
    assert_equal "Institution", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
