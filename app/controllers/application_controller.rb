class ApplicationController < ActionController::Base
  before_action :authenticate_user!, only: [:school_parents, :history_of_arts]
  before_action :set_locale
  def index
    @news = Post.limit(3).order(created_at: :desc)
  end

  def contact
    @contact = Contact.new
  end

  def set_locale
    if cookies[:imde_locale] && I18n.available_locales.include?(cookies[:imde_locale].to_sym)
      l = cookies[:imde_locale].to_sym
    else
      l = I18n.default_locale
      cookies.permanent[:imde_locale] = l
    end
    I18n.locale = l
    @lang = l
  end


  def contact_us
    @contact = Contact.new(contact_params)
    if @contact.valid?
      @contact.save
      ContactMailer.contact_us(@contact).deliver_now
      ContactMailer.contact_user(@contact).deliver_now
    end
    redirect_to thank_you_path
  end

  def school_parents
    @lessons = ParentLesson.all
  end

  def history_of_arts
    @lessons = HistoryLesson.all
  end

  def thank_you_contact
    # prepare_meta_tags title: "Gracias", description: "una breve descripcion de la pagina de agradecimiento"
  end

  def change_locale
    l = params[:locale].to_s.strip.to_sym
    l = I18n.default_locale unless I18n.available_locales.include?(l)
    cookies.permanent[:imde_locale] = l
    redirect_to request.referer || root_url
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path
  end

  private
  def contact_params
    params.require(:contact).permit(:name, :email, :phone, :comment)
  end

  def after_sign_in_path_for(resource)
    unless current_user.password_changed
      edit_passwords_path
    else
      if current_user.has_role? :admin
        users_path
      else
        request.env['omniauth.origin'] || stored_location_for(resource) || root_path
      end
    end
  end

  def get_config
    @config = Setup.first
  end


end
