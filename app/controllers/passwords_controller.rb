class PasswordsController < ApplicationController
  def edit
    @minimum_password_length = 6
  end

  def update
    user = current_user
    user.password_changed = true
    user.save
    if current_user.update_with_password(user_params)
      flash[:notice] = 'password update succeed..'
      sign_in user
      redirect_to new_user_session_path
    else
      flash[:error] = 'password update failed.'
      render :edit
    end
  end

  private
    def user_params
      params.require(:user).permit(:current_password, :password, :password_confirmation)
    end
end
