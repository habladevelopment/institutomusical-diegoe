class ApplicationMailer < ActionMailer::Base
  default from: 'Instituto Musical Diego Echavarria <info@diegoechavarria.com>'
  layout 'mailer'
end
