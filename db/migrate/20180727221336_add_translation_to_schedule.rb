class AddTranslationToSchedule < ActiveRecord::Migration[5.2]
  def change
    add_column :schedules, :description_en, :string
  end
end
