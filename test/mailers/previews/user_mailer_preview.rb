# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/contact_new_user
  def contact_new_user
    UserMailer.contact_new_user(User.first)
  end

end
