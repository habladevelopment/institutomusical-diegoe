class CreateSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :schedules do |t|
      t.date :date_event
      t.string :description

      t.timestamps
    end
  end
end
