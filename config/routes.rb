Rails.application.routes.draw do
  
  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all
  get 'passwords/edit'

  get 'schedule/index'

  devise_for :users, :skip => [:registrations], controllers: { passwords: 'my_passwords' }
  root "application#index"
  get "/about_us", to: "application#about_us" , as: "about_us"
  get '/graduates', to: 'graduated#index', as: 'graduated'
  get '/payments-and-donations', to: 'application#payments', as: 'payments'

  get '/contact', to: 'application#contact', as: 'contact'
  get '/inscriptions', to: 'inscriptions#index', as: 'inscriptions'
  post '/inscription/create', to: 'inscriptions#create', as: "inscription_create"
  get '/circulars', to: 'circulars#index', as: 'circulars'    

  post '/contact_us', to: 'application#contact_us', as: 'contact_us'
  get '/contact_us', to: 'application#contact'
  get '/thank-you', to: 'application#thank_you_contact', as: 'thank_you'
  get '/inscriptions/thank-you', to: 'inscriptions#thank_you', as: 'inscriptions_thank_you'


  get 'circular', to: 'circular#index', as: 'ciruclar'
  get '/parents-school', to: 'application#school_parents', as: "school_parents"
  get '/history-and-appretiation-of-arts', to: 'application#history_of_arts', as: "history_of_arts"

  get '/news', to: 'news#index', as: 'news'
  get '/news/search/:term', to: 'news#search', as: 'news_search'
  get '/news/:id', to: 'news#show', as: 'news_show'
  get '/coexistence-manual', to: 'coexistence_manual#index', as: 'coexistence_manual'

  get "schedules", to: "schedules#index", as: "schedules_front"

  get '/faq', to: 'application#faq', as: 'faq'
  
  resource :passwords

  get '/change_locale/:locale', to: 'application#change_locale', as: 'change_locale'
  get "admin", to: "users#index", as: "admin"

  scope :admin do
    get "schedule/", to: "schedules#index_admin", as: "schedules_admin"
    resources :schedules
    get '/users', to: 'users#index', as: 'users'
    post '/users', to: 'users#create', as: 'users_create'
    get '/users/import', to: 'users#index_import', as: 'show_import_user'
    get '/users/new', to: 'users#new', as: 'new_user'
    get '/users/:id/edit', to: 'users#edit', as: 'edit_user'
    get '/users/:id', to: 'users#show', as: 'user'
    patch '/users/:id', to: 'users#update', as: 'update_user'
    put '/users/:id', to: 'users#update'
    delete '/users/:id', to: 'users#destroy', as: 'destroy_user'
    post '/users/import', to: 'users#import', as: 'import_users'

    get "category_post", to: "category_posts#index_admin", as: "category_posts_admin"
    get "category_posts/new", to: "category_posts#new", as: "new_category_posts"
    get "category_posts/edit/:id", to: "category_posts#edit", as: "edit_category_posts"
    post "category_posts", to: "category_posts#create"
    patch "category_posts/update/:id", to: "category_posts#update", as: "update_category_posts"
    delete "category_posts/:id", to: "category_posts#destroy", as: "destroy_category_posts"

    get "post", to: "posts#index_admin", as: "posts_admin"
    get "posts/new", to: "posts#new", as: "new_posts"
    get "posts/edit/:slug", to: "posts#edit", as: "edit_posts"
    post "posts", to: "posts#create"
    patch "posts/update/:slug", to: "posts#update", as: "update_posts"
    delete "posts/:slug", to: "posts#destroy", as: "destroy_posts"

    get "parents-lesson", to: "parent_lessons#index_admin", as: "parents_school_lessons_admin"
    get "parents-lesson/new", to: "parent_lessons#new", as: "new_parents_school_lessons"
    get "parents-lesson/edit/:id", to: "parent_lessons#edit", as: "edit_parents_school_lessons"
    post "parents-lesson", to: "parent_lessons#create_record"
    patch "parents-lesson/update/:id", to: "parent_lessons#update", as: "update_parents_school_lessons"
    delete "parents-lesson/:id", to: "parent_lessons#destroy", as: "destroy_parents_school_lessons"

    get "history-of-arts-lesson", to: "history_lessons#index_admin", as: "history_lessons_admin"
    get "history-of-arts-lesson/new", to: "history_lessons#new", as: "new_history_lessons"
    get "history-of-arts-lesson/edit/:id", to: "history_lessons#edit", as: "edit_history_lessons"
    post "history-of-arts-lesson", to: "history_lessons#create_record", as: "history_create"
    patch "history-of-arts-lesson/update/:id", to: "history_lessons#update", as: "update_history_lessons"
    delete "history-of-arts-lesson/:id", to: "history_lessons#destroy", as: "destroy_history_lessons"

    get "home_image", to: "home_images#index_admin", as: "home_images_admin"
    get "home_images/new", to: "home_images#new", as: "new_home_images"
    get "home_images/edit/:id", to: "home_images#edit", as: "edit_home_images"
    post "home_images", to: "home_images#create"
    patch "home_images/update/:id", to: "home_images#update", as: "update_home_images"
    delete "home_images/:id", to: "home_images#destroy", as: "destroy_home_images"
    patch "home_images/change_status/:id", to: "home_images#change_status", as: "change_status_home_images"

    #resources :setups
    get 'setup', to: "setups#edit", as: "edit_setup"
    patch 'setup', to: "setups#update", as: "setup_update"
    post 'setup', to: "setups#edit", as: "setup"
    
    get "circulars", to: "circulars#index_admin", as: 'circulars_admin'
    get "circulars/new", to: "circulars#new_admin", as: "create_circular"
    get "circulars/:id/edit", to: "circulars#edit_admin", as: "edit_circular"
    patch "circulars/:id", to: "circulars#update", as: "circulars_update"
    post "circulars", to: "circulars#create", as: "circulars_create"

  end

  # get "/*path", to: redirect("/#{I18n.default_locale}/%{path}", status: 302), constraints: {path: /(?!(#{I18n.available_locales.join("|")})\/).*/}, format: false, unless: "/admin/*path"

end
