class CategoryPostsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_category_post, only: [:show, :edit, :update, :destroy]

  # GET /schedules
  # GET /schedules.json
  layout 'admin', only: [:index_admin, :new, :edit]
  def index_admin
    @category_posts = CategoryPost.all
  end

  # GET /schedules/1
  # GET /schedules/1.json
  def show
  end

  # GET /schedules/new
  def new
    @category_post = CategoryPost.new
  end

  # GET /schedules/1/edit
  def edit
  end

  # POST /schedules
  # POST /schedules.json
  def create
    @category_post = CategoryPost.new(category_post_params)

    respond_to do |format|
      if @category_post.save
        format.html { redirect_to category_posts_admin_path, notice: 'Category post was successfully created.' }
        format.json { render :show, status: :created, location: @category_post }
      else
        format.html { render :new }
        format.json { render json: @category_post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schedules/1
  # PATCH/PUT /schedules/1.json
  def update
    respond_to do |format|
      if @category_post.update(category_post_params)
        format.html { redirect_to category_posts_admin_path, notice: 'Category post was successfully updated.' }
        format.json { render :show, status: :ok, location: @category_post }
      else
        format.html { render :edit }
        format.json { render json: @category_post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schedules/1
  # DELETE /schedules/1.json
  def destroy
    @category_post.destroy
    respond_to do |format|
      format.html { redirect_to category_posts_admin_path, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category_post
      @category_post = CategoryPost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_post_params
      params.require(:category_post).permit(:name, :name_en)
    end
end
