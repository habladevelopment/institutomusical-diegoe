# Preview all emails at http://localhost:3000/rails/mailers/inscription_mailer
class InscriptionMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/inscription_mailer/applicant
  def applicant
    InscriptionMailer.applicant(Inscription.first)
  end

  # Preview this email at http://localhost:3000/rails/mailers/inscription_mailer/institution
  def institution
    InscriptionMailer.institution(Inscription.first)
  end

  def priority
    InscriptionMailer.priority(Inscription.first)
  end

end
