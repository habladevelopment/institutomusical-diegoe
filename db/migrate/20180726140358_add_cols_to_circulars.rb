class AddColsToCirculars < ActiveRecord::Migration[5.2]
  def change
    add_column :circulars, :date, :datetime
    add_column :circulars, :title, :string
    add_column :circulars, :content, :string
  end
end
