class HomeImagesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_home_image, only: [:show, :edit, :update, :destroy, :change_status]

  layout 'admin', only: [:index_admin, :new, :edit]
  def index_admin
    @images_active = HomeImage.where(status: true)
    @images_inactive = HomeImage.where(status: false)
  end

  def show
  end

  def new
    @image = HomeImage.new
  end

  def edit
  end

  def create
    @image = HomeImage.new(home_image_params)
    respond_to do |format|
      if @image.save
        format.html { redirect_to home_images_admin_path, notice: 'banner was successfully created.' }
        format.json { render :show, status: :created, location: @image }
      else
        format.html { render :new }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @image.update(home_image_params)
        format.html { redirect_to home_images_admin_path, notice: 'banner was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @image.destroy
    respond_to do |format|
      format.html { redirect_to home_images_admin_path, notice: 'banner was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def change_status
    if @image.status
      @image.status = false
    else
      @image.status = true
    end
    @image.save
    respond_to do |format|
      format.html { redirect_to home_images_admin_path, notice: 'banner was successfully updated.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_home_image
      @image = HomeImage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def home_image_params
      params.require(:home_image).permit(:image, :status)
    end
end
