class HomeImage < ApplicationRecord
  resourcify
  
  has_attached_file :image
	validates :image, attachment_presence: false
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
end
