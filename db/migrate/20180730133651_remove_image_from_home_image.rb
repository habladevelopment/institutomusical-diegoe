class RemoveImageFromHomeImage < ActiveRecord::Migration[5.2]
  def change
    remove_column :home_images, :image, :string
  end
end
