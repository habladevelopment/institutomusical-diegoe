class User < ApplicationRecord
  rolify
  require 'csv'
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, password_length: 1..128
  def self.import(file)
    puts "*****************"
    puts "*****************"
    puts "*****************"
    pp file
    puts "*****************"
    puts "*****************"
    puts "*****************"
    puts "*****************"
    pp File.read(file.path).encoding
    puts "*****************"
    puts "*****************"
    puts "*****************"
    CSV.foreach(file.path, :headers => true, :encoding => 'ISO-8859-1', :col_sep => ",") do |row|
      user_hash = row.to_hash
      puts "*****************"
      puts "-------- Usuario --------"
      puts "*****************"
      pp user_hash
      @user = User.new(user_hash)
      @user.email = @user.email.strip unless @user.email.nil?
      @user.password = @user.password.strip unless @user.password.nil?
      @user.password_confirmation = @user.password_confirmation.strip unless @user.password_confirmation.nil?
      @user.save
    end
  end
end
