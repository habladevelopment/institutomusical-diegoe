// Create a new instance of an audio object and adjust some of its properties
var audio = new Audio();
audio.src = '/assets/front/songs/1.mp3';
audio.controls = true;
audio.loop = true;
audio.autoplay = false;

var isPaused = true;
// Establish all variables that your Analyser will use
var canvas, ctx, canvas2, ctx2, source, context, analyser, fbc_array, bars, bar_x, bar_width, bar_height;
// Initialize the MP3 player after the page loads all of its HTML into the window
window.addEventListener("load", initMp3Player, false);
function initMp3Player(){
  document.getElementById('audio_box').appendChild(audio);
  context = new AudioContext(); // AudioContext object instance
  analyser = context.createAnalyser(); // AnalyserNode method
  canvas = document.getElementById('analyser_render');
  ctx = canvas.getContext('2d');
  canvas2 = document.getElementById('analyser_render_2');
  ctx2 = canvas2.getContext('2d');
  // Re-route audio playback into the processing graph of the AudioContext
  source = context.createMediaElementSource(audio); 
  source.connect(analyser);
  analyser.connect(context.destination);
  frameLooper();
}
// frameLooper() animates any style of graphics you wish to the audio frequency
// Looping at the default frame rate that the browser provides(approx. 60 FPS)
function frameLooper(){
  window.requestAnimationFrame(frameLooper);
  fbc_array = new Uint8Array(analyser.frequencyBinCount);
  analyser.getByteFrequencyData(fbc_array);
  ctx.clearRect(0, 0, canvas.width, canvas.height); // Clear the canvas
  ctx.fillStyle = '#f7eab5'; // Color of the bars
  ctx2.clearRect(0, 0, canvas.width, canvas.height); // Clear the canvas
  ctx2.fillStyle = '#f7eab5'; // Color of the bars
  bars = 75;
  for (var i = 0; i < bars; i++) {
    bar_x = i * 4;
    bar_width = 2;
    bar_height = -(fbc_array[i] / 2);
    //  fillRect( x, y, width, height ) // Explanation of the parameters below
    ctx.fillRect(bar_x, canvas.height, bar_width, bar_height);
    ctx2.fillRect(bar_x, canvas.height, bar_width, bar_height);
  }
}

function playStopAudio(){
  if(isPaused) {
    audio.play();
    $('.control-player.play').fadeOut();
    $('.control-player.stop').fadeIn();
  } else {
    audio.pause();
    $('.control-player.stop').fadeOut();
    $('.control-player.play').fadeIn();
  }
  isPaused = !isPaused;
  changeAudioState(isPaused);
  playerControlsPosition(isPaused);
}

var actualSong = 1;
var totalSongs = 2;
var songs = ["Serenata n 13", "Las bodas de Figaro"]

function previousSong(){
  if(actualSong>1) {
    actualSong-=1;
  } else {
    actualSong = totalSongs;
  }

  audio.src = '/assets/front/songs/'+actualSong+'.mp3';
  loadAudio();
}

function nextSong(){
  if(actualSong<totalSongs) {
    actualSong+=1;
  } else {
    actualSong = 1;
  }

  audio.src = '/assets/front/songs/'+actualSong+'.mp3';
  loadAudio();
}

function loadAudio() {
  isPaused = false;
  audio.load(); //call this to just preload the audio without playing
  changeNameAudio(songs[actualSong-1])
  changeAudioState(isPaused);
  playerControlsPosition(isPaused);
  audio.play(); //call this to play the song right away
  $('.control-player.play').fadeOut();
  $('.control-player.stop').fadeIn();
}

function changeNameAudio(name){
  $('.audio-info span.audio-name').text(name)
}

function changeAudioState(paused){
  if (paused) {
    $('.audio-info span.state').text("Off");
  } else {
    $('.audio-info span.state').text("On");
  }
}

function playerControlsPosition(paused) {
  if (paused) {
    $('.nav-audio-control').removeClass("playing");
    $('.audio-info').removeClass("playing");
  } else {
    $('.nav-audio-control').addClass("playing");
    $('.audio-info').addClass("playing");
  }
}