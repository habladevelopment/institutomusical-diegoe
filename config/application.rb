require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module InstitutoMusical
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')

    #config.time_zone = 'Mexico City'
    I18n.available_locales = [:en, :es]
    config.i18n.default_locale = :es

    config.exceptions_app = self.routes

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default :charset => "utf-8"
    config.action_mailer.smtp_settings = {
      :address        => 'smtp.sendgrid.net',
      :port           => 587,
      :domain         => 'insitutomusicaldiegoechavarria.com',
      :authentication => 'plain',
      :enable_starttls_auto => true,
      :user_name      =>  "sendgridhablageneral@gmail.com",
      :password       =>  "Habla2020*"
    }

  end
end
