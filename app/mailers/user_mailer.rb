class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.contact_new_user.subject
  #
  def contact_new_user(user)
    @user = user

    mail(to: user.email, subject: "Bienvenido al Instituto Musical Diego Echavarría")
  end
end
