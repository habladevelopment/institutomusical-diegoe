class AddEnColsToCirculars < ActiveRecord::Migration[5.2]
  def change
    add_column :circulars, :title_en, :string
    add_column :circulars, :content_en, :string
  end
end
