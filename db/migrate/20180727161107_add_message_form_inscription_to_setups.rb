class AddMessageFormInscriptionToSetups < ActiveRecord::Migration[5.2]
  def change
    add_column :setups, :message_form_inscription, :string
    add_column :setups, :message_form_inscription_english, :string
  end
end
