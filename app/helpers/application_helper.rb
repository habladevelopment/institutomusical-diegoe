module ApplicationHelper
    def title(text)
        content_for :title, text
    end

    def description(text)
        content_for :description, text
    end

    def is_locale_en
    return I18n.locale == :en
    end

    def is_locale_es
    return I18n.locale == :es
    end
end
