class CircularsController < ApplicationController 
    before_action :authenticate_user!
    load_and_authorize_resource
    layout 'admin', only: [:index_admin, :new_admin, :edit_admin]
    before_action :find_circular, only: [:edit_admin, :update]
    before_action :find_all_circulars, only: [:index, :index_admin]

    def index_admin
    end
    
    def new_admin
        @circular = Circular.new
    end
    
    def index
    end

    def create
        @circular = Circular.new(circular_params)
        puts "******"
        puts @circular
        if @circular.valid?
            @circular.save
            redirect_to circulars_admin_path
        end
    end

    def edit_admin        
    end

    def update
        @circular.update(circular_params)
        redirect_to circulars_admin_path
    end

    private
        def circular_params
            params.require(:circular).permit(:date, :title, :content, :content_en, :title_en)
        end

        def find_circular
            @circular = Circular.find(params[:id])
        end

        def find_all_circulars
            @circulars = Circular.all
        end
end
