class UsersController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

    require 'csv'
    layout 'admin', only: [:index, :new, :edit, :show, :index_import]
    before_action :set_user, only: [:show, :edit, :update, :destroy]
    
    # GET /tests
    # GET /tests.json
    def index
        unless current_user.has_role? :admin
            redirect_to root_path
        end
        @users = User.all
        puts "*******"
        puts @users
    end
    
    # GET /tests/1
    # GET /tests/1.json
    def show
    end
    
    # GET /tests/new
    def new
        @user = User.new
    end
    
    # GET /tests/1/edit
    def edit
    end
    
    # POST /tests
    # POST /tests.json
    def create
        @user = User.new(user_params)
        
        respond_to do |format|
            if @user.save
                format.html { redirect_to @user, notice: 'User was successfully created.' }
                format.json { render action: 'show', status: :created, location: @user }
            else
                format.html { render action: 'new' }
                format.json { render json: @test.errors, status: :unprocessable_entity }
            end
        end
    end
    
    # PATCH/PUT /tests/1
    # PATCH/PUT /tests/1.json
    def update
        respond_to do |format|
            user_req = nil
            if user_params[:password].blank?
                user_req = @user.update_without_password(user_params)
            else
                user_req = @user.update(user_params)
            end
            if user_req
                format.html { redirect_to @user, notice: 'User was successfully updated.' }
                format.json { head :no_content }
            else
                format.html { render action: 'edit' }
                format.json { render json: @user.errors, status: :unprocessable_entity }
            end
        end
    end
    
    # DELETE /tests/1
    # DELETE /tests/1.json
    def destroy
        @user.destroy
        respond_to do |format|
            format.html { redirect_to users_url }
            format.json { head :no_content }
        end
    end

    def index_import
    end

    def import
        User.import(params[:file])
        redirect_to users_path, notice: "imported correctly"
    end
    
    private
    # Use callbacks to share common setup or constraints between actions.
        def set_user
            @user = User.find(params[:id])
        end
        
        # Never trust parameters from the scary internet, only allow the white list through.
        def user_params
            params.require(:user).permit(:name, :email, :password, :password_confirmation)
        end
end
