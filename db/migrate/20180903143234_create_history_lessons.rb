class CreateHistoryLessons < ActiveRecord::Migration[5.2]
  def change
    create_table :history_lessons do |t|
      t.string :video
      t.attachment :thumbnail

      t.timestamps
    end
  end
end
