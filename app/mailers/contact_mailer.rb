class ContactMailer < ApplicationMailer
  before_action :get_emails_from_setup
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.inscription_mailer.applicant.subject
  #
  def contact_us(user)
    @user = user
    # mail(to: [ENV['EMAIL_TO_CONTACT_US'].split(",").map(&:strip)], subject: '')
    mail(to: @user.email, subject: '!Gracias por comunicarte con nosotros!')
  end
  
  def contact_user(user)
    @user = user
    mail(to: @emails_to_send, subject: 'Contacto desde el sitio web')
    # mail(to: 'ti@hablacreativo.com', subject: '')
  end

  def get_emails_from_setup
    @emails_to_send = Setup.first.emails_notification_contact.split(",").map(&:strip)
  end

end
