class AddNamesToParentLesson < ActiveRecord::Migration[5.2]
  def change
    add_column :parent_lessons, :name_es, :string
    add_column :parent_lessons, :name_en, :string
  end
end
