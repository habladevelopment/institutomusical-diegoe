class MyPasswordsController < Devise::PasswordsController
  before_action :merge_locale

  def merge_locale
    resource_params.merge!(locale: 'en') # use 'en' for eg
  end
end