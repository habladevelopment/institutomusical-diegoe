class AddNamesToHistoryLesson < ActiveRecord::Migration[5.2]
  def change
    add_column :history_lessons, :name_es, :string
    add_column :history_lessons, :name_en, :string
  end
end
