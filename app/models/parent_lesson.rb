class ParentLesson < ApplicationRecord
  has_attached_file :thumbnail
	validates :thumbnail, attachment_presence: false
  validates_attachment_content_type :thumbnail, content_type: /\Aimage\/.*\Z/
end
