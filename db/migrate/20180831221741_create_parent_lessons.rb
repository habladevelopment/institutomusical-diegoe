class CreateParentLessons < ActiveRecord::Migration[5.2]
  def change
    create_table :parent_lessons do |t|
      t.string :video
      t.attachment :thumbnail

      t.timestamps
    end
  end
end
