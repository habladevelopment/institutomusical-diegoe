class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title
      t.string :content
      t.string :author
      t.string :date
      t.attachment :image

      t.timestamps
    end
  end
end
