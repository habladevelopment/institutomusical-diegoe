class AddEmailsNotificationPriorityToSetup < ActiveRecord::Migration[5.2]
  def change
    add_column :setups, :emails_notification_priority, :string
  end
end
