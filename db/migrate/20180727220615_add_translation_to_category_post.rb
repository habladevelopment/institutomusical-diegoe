class AddTranslationToCategoryPost < ActiveRecord::Migration[5.2]
  def change
    add_column :category_posts, :name_en, :string
  end
end
