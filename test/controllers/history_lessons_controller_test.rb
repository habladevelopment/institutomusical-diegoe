require 'test_helper'

class HistoryLessonsControllerTest < ActionDispatch::IntegrationTest
  test "should get index_admin" do
    get history_lessons_index_admin_url
    assert_response :success
  end

  test "should get new" do
    get history_lessons_new_url
    assert_response :success
  end

  test "should get edit" do
    get history_lessons_edit_url
    assert_response :success
  end

end
