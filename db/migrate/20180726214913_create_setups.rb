class CreateSetups < ActiveRecord::Migration[5.2]
  def change
    create_table :setups do |t|
      t.boolean :active_form_inscription
      t.string :emails_notification_contact
      t.string :emails_notification_inscription

      t.timestamps
    end
  end
end
