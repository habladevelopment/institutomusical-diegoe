class CreateInscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :inscriptions do |t|
      t.string :city
      t.string :neighborhood
      t.string :address
      t.string :phone
      t.string :another_phone
      t.string :change_reason
      t.string :who_recommended
      t.string :student_name
      t.string :student_last_school
      t.string :student_birthdate
      t.string :student_ingress_year
      t.string :mother_name
      t.string :mother_age
      t.string :mother_school
      t.string :mother_profession
      t.string :mother_university
      t.string :mother_workplace
      t.string :mother_job
      t.string :mother_email
      t.string :mother_phone
      t.string :mother_cellphone
      t.string :father_name
      t.string :father_age
      t.string :father_school
      t.string :father_profession
      t.string :father_university
      t.string :father_workplace
      t.string :father_job
      t.string :father_email
      t.string :father_phone
      t.string :father_cellphone

      t.timestamps
    end
  end
end
