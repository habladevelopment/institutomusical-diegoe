class AddPwChangedToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :password_changed, :boolean, default: false
  end
end
