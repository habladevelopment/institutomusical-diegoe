module ParentLessonsHelper
  def parent_lesson_video(lesson)
    if I18n.locale == :es
      video = lesson.video
    else
      video = lesson.video_en
    end
    video
  end

  def parent_lesson_name(lesson)
    if I18n.locale == :es
      name = lesson.name_es
    else
      name = lesson.name_en
    end
    name
  end
end
